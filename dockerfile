###################################################
####################BWWS###########################
###################################################

FROM python:latest

ENV PYTHONUNBUFFERED 1

RUN apt-get install -y libssl-dev dpkg-dev 

RUN mkdir /code
WORKDIR /code
COPY . /code/

#installs python dependency
RUN pip install --upgrade pip
RUN pip install -r ./requirements.txt

#migrate data and start server

CMD ["sh" ,"/code/prod_deploy.sh" ]