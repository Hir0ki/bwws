from django.contrib import admin
from .models import Lieferanten, Produkt
# Register your models here.

admin.site.register(Lieferanten)
admin.site.register(Produkt)