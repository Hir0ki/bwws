from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from .views import (
    LieferantenQuantitativ,
    Home,
    LieferantenQualitativ,
    ProduktViewSet,
    LieferantenViewSet,
    Lagerkennziffern,
    Optimale_bestellmenge,
    UserInfo,
    ABC_Analyse,
    Stammdaten,
    Impressum,
)

router = routers.DefaultRouter()
router.register(r"produkt", ProduktViewSet)
router.register(r"lieferant", LieferantenViewSet)

urlpatterns = [
    path("liefer_quali/", LieferantenQualitativ.as_view()),
    path("liefer_quanti/", LieferantenQuantitativ.as_view()),
    path("kennziffern/", Lagerkennziffern.as_view()),
    path("optimale_bestellmenge/", Optimale_bestellmenge.as_view()),
    path("", Home.as_view()),
    path("api/", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="restframework")),
    path('abc_analyse/', ABC_Analyse.as_view()),
    path("show_user/", UserInfo.as_view()),
    path("stammdaten/", Stammdaten.as_view()),
    path('impressum/', Impressum.as_view())
]


