from rest_framework import serializers
from .models import Produkt, Lieferanten

class ProduktSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Produkt
        fields = ('id', 'name', 'ek_preis', 'list_preis',
                  'beschreibung', 'verbrauchsmenge',
                  'lagerkosten', 'bestellkosten', 'bestand', 'erdat' )



class LieferantenSerializers(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lieferanten
        fields = ('id', 'name', 'rabatt', 'skonto', 'erdat' )

