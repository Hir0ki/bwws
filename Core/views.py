from django.shortcuts import render
from django.views import View
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BaseAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from .models import Lieferanten, Produkt
from Core.serializers import ProduktSerializers, LieferantenSerializers
from django.contrib.auth.models import User


class Home(View):
    def get(self, request):
        return render(request, "base.html")

class UserInfo(View):
    def get(self, request):
        user = None
        context = {}
        out = []
        if request.user.is_authenticated:
            user = request.user
            permissions = user.get_all_permissions()

            for p in permissions:
                l = str(p).split(".")[1].split("_")
                # translations
                if l[0] == "change":
                    l[0] = "ändern"
                elif l[0] == "add":
                    l[0] = "hinzfügen"
                elif l[0] == "delete":
                    l[0] = "löschen"
                elif l[0] == "view":
                    l[0] = "anzeigen"
                out.append(l[1] + " " + l[0])
            out.sort()



        context = {
            "permissions": out
        }
        return render(request, "Core/UserInfo.html", context)

class Stammdaten(View):
    def get(self, request):
        return render(request, 'Core/Stammdaten.html')

class LieferantenQuantitativ(View):
    def get(self, request):
        return render(request, 'Core/Lieferanten_quantitativ.html')

class LieferantenQualitativ(View):

    def get(self, request):
        return render(request, 'Core/Lieferanten_qualitativ.html' )

class ABC_Analyse(View):

    def get(self,request):
        return render(request, 'Core/ABC_Analyse.html')

class Lagerkennziffern(View):

    def get(self, request):
        return render(request, 'Core/Lagerkennziffern.html')

class Optimale_bestellmenge(View):
    def get(self, request):
        return render(request, 'Core/Optimale_Bestellmenge.html')


class Impressum(View):
    def get(self, request):
        return render(request, 'Core/Impressum.html' )

class ProduktViewSet(viewsets.ModelViewSet):
    queryset = Produkt.Produkt.all()
    serializer_class = ProduktSerializers

class LieferantenViewSet(viewsets.ModelViewSet):
    queryset = Lieferanten.Lieferant.all()
    serializer_class = LieferantenSerializers


