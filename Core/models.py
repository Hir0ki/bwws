from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Lieferanten(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    rabatt = models.IntegerField(
        default=0,
        validators=[MaxValueValidator(100), MinValueValidator(-1)]
    )
    skonto = models.IntegerField(
        default=0,
        validators=[MaxValueValidator(100), MinValueValidator(-1)]
    )

    erdat = models.DateField(verbose_name="Erstellungsdatum", auto_now=True)

    Lieferant = models.Manager()

    def __str__(self):
        return self.name


class Produkt(models.Model):
    id = models.IntegerField(primary_key=True, verbose_name="Artikelnummer")
    name = models.CharField(max_length=255, verbose_name="Bezeichnung")
    ek_preis = models.DecimalField(
        max_digits=30,
        decimal_places=2,
        verbose_name="EK-Preis",
        validators=[MinValueValidator(0.01)],
        default=1,
    )
    list_preis = models.DecimalField(
        max_digits=50,
        decimal_places=2,
        verbose_name="Verkaufspreis pro Stück",
        validators=[MinValueValidator(1)],
    )
    beschreibung = models.TextField(max_length=200)
    verbrauchsmenge = models.IntegerField(
        verbose_name="Verbrauchsmenge in Stück",
        validators=[MinValueValidator(1)],
        default=1,
    )
    lagerkosten = models.DecimalField(
        max_digits=6,
        decimal_places=1,
        verbose_name="Lagerhaltungskosten",
        validators=[MinValueValidator(0.1)],
        default=1,
    )
    bestellkosten = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        verbose_name="Bestellkosten Pro Bestellung",
        validators=[MinValueValidator(0.01)],
        default=1,
    )
    bestand = models.IntegerField(
        verbose_name="Bestand",
        validators=[MinValueValidator(1)],
        default=1,
    )
    erdat = models.DateField(verbose_name="Erstellungsdatum", auto_now=True)

    Produkt = models.Manager()

    def __str__(self):
        return self.name

