async function get_api_data(url) {
    let base_url = window.location.origin + url;
    const response = await fetch(base_url);
    const myJson = response.json(); //extract JSON from the http response
    return myJson
}

function getProzent(value, max, min) {
    max = max - min
    value = value - min

    let prozent = value / (max / 100);

    return prozent

}


function getColor(value, max, min) {
    let prozent = getProzent(value = value, max = max, min = min)
    g = 255 - prozent * 2.5;
    r = prozent * 2.5;

    if (g > 255) {
        g = 255
    }

    if (r > 255) {
        r = 255
    }
    return 'rgb(' + r + ',' + g + ',0)';
}


function sort_data_table(data, key, asc) {
    let minIdx, temp
    len = data.length;
    for (let i = 0; i < len; i++) {
        minIdx = i;
        for (let j = i + 1; j < len; j++) {
            if (Number(data[j][key]) < Number(data[i][key])) {
                minIdx = j;
            }
        }
        temp = data[i]
        data[i] = data[minIdx]
        data[minIdx] = temp;
    }

    if (asc == true) {
        data.reverse()
    }
    return data
}


function convert_data_entrys_to_number(data, keys, decimalplaces) {
    data.forEach(entry => {
        for (i = 0; i < keys.length; i++) {
            entry[keys[i]] = Number(entry[keys[i]]).toFixed(decimalplaces)
        }
    });
}

function callPopUp() {
    var popup = document.getElementById("myPopup");
    console.log(window.location.pathname);
    switch (window.location.pathname) {
        case "/":
            popup.innerHTML = "Bei weiteren Fragen bitte kontaktieren Sie info@bwws.tk"
            break;
        case "/stammdaten/":
            popup.innerHTML = "Mithilfe der oberen Auswahlleiste (“Tabelle wählen”) wählen Sie zunächst, ob Sie den Stammdatensatz der eingepflegten Produkte oder der Lieferanten ansehen möchten. Klicken Sie dazu mit der linken Maustaste auf die Auswahlleiste und wählen Sie im nun erscheinenden Drop-Down-Menü zwischen “Produkt” und “Lieferant”. In der Tabelle darunter finden Sie daraufhin die Stammdaten. Mithilfe der Suchleiste im oberen rechten Eck können Sie in der Tabelle nach einzelnen Produkten suchen. Alternativ können Sie auch auf eines der Kriterien in der Kopfzeile der Tabelle klicken um nach diesem Kriterium zu sortieren. Standardmäßig werden Ihnen beim Aufrufen der Stammdaten nur die zehn ersten Ergebnisse in der Tabelle dargestellt. Um mehr Ergebnisse angezeigt zu bekommen, können Sie entweder oben Links unter “Show entries” die Zahl der maximal angezeigten Ergebnisse erhöhen oder rechts unter der Tabelle mithilfe der beiden Knöpfe “Previous” und “Next” zwischen den Tabellenseiten springen."
            break;
        case "/abc_analyse/":
            popup.innerHTML = "Mit der obersten Auswahlleiste wählen Sie zunächst den Typ der Analyse, hierbei können sie zwischen “Umsatz kumuliert” oder “Menge kumuliert” wählen. Klicken Sie dazu auf die Auswahlleiste und bestätigen Sie Ihre Auswahl indem Sie auf den entsprechenden Eintrag des Drop-Down-Menüs klicken. Mithilfe der beiden Auswahlleisten darunter können Sie nun die Grenzen für die A- und B-Waren definieren. Hierfür klicken Sie in das entsprechende Feld und geben die gewünschten Werte über die Tastatur ein. Nach einem Klick auf Tabelle aktualisieren, sehen Sie in der Tabelle die A-, B- und C-Waren mit den dazugehörigen Kennzahlen. Durch einen Klick auf “Grafik anzeigen” erhalten Sie das Ergebnis in einer Grafik dargestellt. Mithilfe der Suchleiste im oberen rechten Eck können Sie in der Tabelle nach einzelnen Produkten suchen. Alternativ können Sie auch auf eines der Kriterien in der Tabelle klicken um nach diesem Kriterium zu sortieren. Standardmäßig werden Ihnen beim Aufrufen der Stammdaten nur die zehn ersten Ergebnisse in der Tabelle dargestellt. Um mehr Ergebnisse angezeigt zu bekommen, können Sie entweder oben Links unter “Show entries” die Zahl der maximal angezeigten Ergebnisse erhöhen oder rechts unter der Tabelle mithilfe der beiden Knöpfe “Previous” und “Next” zwischen den Tabellenseiten springen."
            break;
        case "/liefer_quali/":
            popup.innerHTML = "Zunächst geben Sie bitte in der oberen Leiste (“Kriteriumname”) einen Namen für Ihr Bewertungskriterium an und vergeben in der Leiste darunter (“Wertung”) eine Gewichtung  für dieses Kriterium. Durch klicken auf den Knopf “Kriterium hinzufügen” fügen Sie das Kriterium zur Tabelle darunter hinzu. Dies wiederholen Sie bis Sie alle gewünschten Kriterien zur Tabelle hinzugefügt haben und bestätigen dies mit einem Klick auf den “Ergebnis rechnen”. Dadurch wird das Ergebnis errechnet und auf der rechten Seite der Tabelle dargestellt. Mithilfe der Suchleiste im oberen rechten Eck können Sie in der Tabelle nach einzelnen Produkten suchen. Alternativ können Sie auch auf eines der Kriterien in der Tabelle klicken um nach diesem Kriterium zu sortieren. Standardmäßig werden Ihnen beim Aufrufen der Stammdaten nur die zehn ersten Ergebnisse in der Tabelle dargestellt. Um mehr Ergebnisse angezeigt zu bekommen, können Sie entweder oben Links unter “Show entries” die Zahl der maximal angezeigten Ergebnisse erhöhen oder rechts unter der Tabelle mithilfe der beiden Knöpfe “Previous” und “Next” zwischen den Tabellenseiten springen."
            break;
        case "/liefer_quanti/":
            popup.innerHTML = "Bitte geben Sie in der oberen Leiste die Bestellkosten ein. Nach einem Druck auf die Enter-Taste werden die Gesamtkosten berechnet und in der Tabelle unter der Eingabeleiste angezeigt. Mithilfe der Suchleiste im oberen rechten Eck können Sie in der Tabelle nach einzelnen Produkten suchen. Alternativ können Sie auch auf eines der Kriterien in der Tabelle klicken um nach diesem Kriterium zu sortieren. Standardmäßig werden Ihnen beim Aufrufen der Stammdaten nur die zehn ersten Ergebnisse in der Tabelle dargestellt. Um mehr Ergebnisse angezeigt zu bekommen, können Sie entweder oben Links unter “Show entries” die Zahl der maximal angezeigten Ergebnisse erhöhen oder rechts unter der Tabelle mithilfe der beiden Knöpfe “Previous” und “Next” zwischen den Tabellenseiten springen."
            break;
        case "/kennziffern/":
            popup.innerHTML = "Hier sehen Sie die Lagerkennziffern der in den Stammdaten hinterlegten Produkte. Mithilfe der Suchleiste im oberen rechten Eck können Sie in der Tabelle nach einzelnen Produkten suchen. Alternativ können Sie auch auf eines der Kriterien in der Tabelle klicken um nach diesem Kriterium zu sortieren. Standardmäßig werden Ihnen beim Aufrufen der Stammdaten nur die zehn ersten Ergebnisse in der Tabelle dargestellt. Um mehr Ergebnisse angezeigt zu bekommen, können Sie entweder oben Links unter “Show entries” die Zahl der maximal angezeigten Ergebnisse erhöhen oder rechts unter der Tabelle mithilfe der beiden Knöpfe “Previous” und “Next” zwischen den Tabellenseiten springen."
            break;
        case "/optimale_bestellmenge/":
            popup.innerHTML = "Zunächst definieren Sie bitte die maximale Bestellmenge in der oberen Eingabeleiste (“maximale Bestellmenge angeben”). Wenn diese Eingabe getätigt wurde, klicken Sie bitte auf die grüne Plus-Symbol neben dem Produkt, von welchem Sie die optimale Bestellmenge ermitteln möchten. In der nun erscheinenden Untertabelle sehen Sie die Veränderung der Bestellmenge, der Bestellkosten, des durchschnittlichen Bestands, der Lagerhaltungskosten und der Gesamtkosten je nach Anzahl der Bestellungen. Anhand der farblichen Hinterlegung kann schnell die Anzahl an Bestellungen mit den geringsten Gesamtkosten gefunden werden. Aus Gründen der Übersicht endet die Anzeige nach der Anzahl an Bestellungen, bei welcher die Bestellmenge unter 1 fallen würde. Bei nachträglicher Veränderung der maximale Bestellmenge muss zur Berechnung die Untertabelle mithilfe des roten Minuszeichens geschlossen und daraufhin durch klicken auf das grüne Plus wieder geöffnet werden. Mithilfe der Suchleiste im oberen rechten Eck können Sie in der Tabelle nach einzelnen Produkten suchen. Alternativ können Sie auch auf eines der Kriterien in der Tabelle klicken um nach diesem Kriterium zu sortieren. Standardmäßig werden Ihnen beim Aufrufen der Stammdaten nur die zehn ersten Ergebnisse in der Tabelle dargestellt. Um mehr Ergebnisse angezeigt zu bekommen, können Sie entweder oben Links unter “Show entries” die Zahl der maximal angezeigten Ergebnisse erhöhen oder rechts unter der Tabelle mithilfe der beiden Knöpfe “Previous” und “Next” zwischen den Tabellenseiten springen."
            break;
        case "/show_user/":
            popup.innerHTML = "Hier sehen Sie informationen zu Ihrem Account, falls Sie ihren Daten pflegen wollen, kontatieren Sie bitte einen Administrator."
            break;

    }
    popup.classList.toggle("show");
}

function log(log_string) {
    console.log(log_string)
}




class Grafik {

    constructor(div_id, width, hight) {
        this.create_canvas(div_id, width, hight)
    }

    create_canvas(div_id, width, hight) {

        let canvas_div = document.getElementById(div_id)
        this.html_object = document.createElement('canvas', width = width, hight = hight)
        canvas_div.appendChild(this.html_object)

    }
    /**
     * 
     * @param {Array<Object>} talbe_data 
     * @param {String} key_data key for data
     * @param {String} key_labe key for lable
     */
    convert_table_data_to_chart_dataset(table_data, key_data, key_lable) {
        this.lables = []
        this.dataset = []
        table_data.forEach(entry => {
            this.lables.push(entry[key_lable])
            this.dataset.push(Number(entry[key_data]))
        })
    }


    create_dataset( borderColor, key, data, name  ) {
        let single_data = {data: []}
        single_data['borderColor'] = borderColor
        single_data['label'] = String(name)
        single_data['borderWidth'] = 2
        single_data['fill'] = false
        data.forEach(entry => {
            single_data['data'].push({ 'x' : Number(entry['Bestellungsnummer']), 'y' : Number(entry[key])})
        })
        return single_data
    }

}