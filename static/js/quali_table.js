
$(document).ready(() => {
    shown_table = new quali_table('quali_table', columns, '/api/lieferant/')

    // event handler for the button
    document.getElementById('kirtd_add_button').addEventListener('click', () => {
        let name = document.getElementById('kirtd_name').value
        document.getElementById('kirtd_name').value = ''
        if (name != '') {
            let wertung = document.getElementById('kirtd_wertung').value
            if (String(name) != "" || String(wertung) != "") {
                shown_table.add_input_column(name)
                shown_table.add_column({ data: 'wertung' + name, title: 'Wertung' }, wertung)
            }
        }
    })

    document.getElementById('calculate').addEventListener('click', () => {


        shown_table.add_column({ title: "Ergebnis", data: "ergebnis" })
        calculate_wertung()

        shown_table.refresh_table()

    })

});

function save_changes() {
    let index = 0;
    shown_table.table_data.data.forEach(entry => {
        let reg_quali = /quali/
        // get kriterium value from dom 
        Object.keys(entry).forEach(key => {
            let match = key.match(reg_quali)
            if (match != null) {
                let query = document.querySelectorAll('.' + key)[index]
                if (query != undefined) {
                    entry[key] = query.value
                }
            }
        })
        index++;
    });

}


class quali_table extends Table {

    constructor(table_id, columns, api_url) {
        super(table_id, columns, api_url)
    }

    _create_table() {
        log("Creating new table")
        this.table = $('#' + this.table_id).DataTable({
            data: this.table_data.data,
            columns: this.columns,
        });
        log("created table")
    }

    add_input_column(name) {
        var current_name = name
        let columns = {
            title: current_name, data: 'quali' + current_name, width: "1%",
            render: (data, row, tpye) => {
                return "<input type='number' max='10' class='quali" + current_name + "' value='" + data + "' onchange='save_changes()'/>"
            }
        }
        let inner_html = 0
        this.add_column(columns, inner_html)
    }//

    refresh_table() {
        this.table.destroy(true)
        this._create_new_html_table()
        this.clean_table_data()
        this._create_table()
    }


}

let columns = [
    {
        data: "id",
        title: "ID",
        width: "5%",
    },
    {
        data: "name",
        title: "Lieferant",
    },
]


function calculate_wertung() {
    shown_table.table_data.data.forEach(entry => {
        let reg_quali = /quali/
        let ergebnis = 0
        // get kriterium value from dom 
        Object.keys(entry).forEach(key => {
            let match = key.match(reg_quali)
            let real_name = key.replace(reg_quali, '')
            if (match != null) {
                console.log("hello")
                ergebnis = Number(Number(ergebnis) + Number(entry[key]) * Number(entry['wertung' + real_name])).toFixed(2)
            }
        })
        entry['ergebnis'] = ergebnis
    });
}
