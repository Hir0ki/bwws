
$(document).ready(() => {
    shown_table = new quanti_table('quanti_table', columns, '/api/lieferant/')

    // event handler for the button
    document.getElementById('Bestellkosten').addEventListener('change', () => {
        shown_table.refresh_table()
    })

});


class quanti_table extends Table {

    constructor(table_id, columns, api_url) {
        super(table_id, columns, api_url)
    }

    clean_table_data(){
        let data = { }
        data = this.api_data
        let bestellkosten = document.getElementById('Bestellkosten').value
        data.forEach(element => {
            element['skonto_euro'] = Number(element['skonto']   * ( bestellkosten / 100 )).toFixed(2)
            element['rabatt_euro'] = Number(element['rabatt'] * ( bestellkosten / 100 )).toFixed(2)
            element['endkosten'] = Number( bestellkosten - ( Number(element['rabatt_euro']) + Number(element['skonto_euro']) ) ).toFixed(2)
        });
        this.table_data = new Table_data(data)
    }
   
}



let columns = [
    {
        data: "id",
        title: "Lieferantennummer"
    },
    {
        data: "name",
        title: "Lieferant"
    },
    {
        data: "skonto",
        title: "Skonto in %"

    },
    {
        data: "rabatt",
        title: "Rabatt in %"
    },
    {
        data: "skonto_euro",
        title: "Skonto in €"
    },
    {
        data: "rabatt_euro",
        title: "Rabatt in €"
    },
    {
        data: "endkosten",
        title: "Endgültige Kosten"
    },
]
