class Lagerkennziffern extends Table{
    constructor(){
        let table_id = "lager"
        let columns = [ { data: "id" },
                        { data: "name" },
                        { data: "verbrauchsmenge" },
                        { data: "lagerkosten"}]
        let api_url = "/api/produkt"
        super(table_id, columns, api_url)
    }

    _create_table() {
        for( var dic in this.api_data ){
            let tmp = this.api_data[dic]
            tmp["m_bestand"] = this._two_decimals((tmp["bestand"] + (tmp["verbrauchsmenge"] -tmp["verbrauchsmenge"]))/2);
            tmp["lagerkosten"] = this._two_decimals((360 * tmp["m_bestand"]) / tmp["verbrauchsmenge"]);
            tmp["verbrauchsmenge"] = this._two_decimals(360/tmp["lagerkosten"]);
            tmp["lagerzins"] = this._two_decimals((0.0176 * tmp["lagerkosten"]) / 360 * 100);
        }
        console.log(this.columns);
        this.columns.splice(2,0,{data: "m_bestand"});
        this.columns.splice(5,0,{data: "lagerzins"});
        console.log(this.columns);
        this.table = $('#' + this.table_id).DataTable({
            data: this.api_data,
            columns: this.columns
        });
    }

    _two_decimals(number){
        return number.toFixed(2);
    }
}

var table =  new Lagerkennziffern()