// locale event handler

$(document).ready(function () {
    create_optimale_table();
});

var bestellmengen


// functionen

function format(data) {
    let output_string = '<table id=subtable cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;"  class="cf">';

    bestellmengen = calculate_optimal_bestellmenge(data.lagerkosten, data.bestellkosten)
    output_string = output_string +
        '<thead>' +
        '<tr>' +
        '<th>Anzahl Bestellungen</th>' +
        '<th>Bestellmenge</th>' +
        '<th>Bestellkosten</th>' +
        '<th>Durschn.Lagerbestand</th>' +
        '<th>Lagerhaltungskosten</th>' +
        '<th>Kosten gesamt</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>'


    bestellmengen.forEach(object => {
        output_string = output_string +
            '<tr data-value=' + object['Kosten gesamt'] + '>' +
            '<td>' + object['Bestellungsnummer'] + '</td>' +
            '<td>' + object['Bestellungsmenge'] + '</td>' +
            '<td>' + object['Bestellungskosten'] + '</td>' +
            '<td>' + object['Durschn.Lagerbestand'] + '</td>' +
            '<td>' + object['Lagerhaltungskosten'] + '</td>' +
            '<td class="for_color_calu">' + object['Kosten gesamt'] + '</td>' +
            '</tr>' +
            '<tr>' +
            '<div class="chart-container" height:20vh; width:30vw" id="div_canvas"></div>' +
            '</tr>'


    });

    return output_string + '</tbody>' + '</table>'

};


function calculate_optimal_bestellmenge(lagerkosten, bestellkosten) {
    let bestellmenge = $("#bestellmenge")[0].value;
    let maximaleBestellungne = $('#maxbestellungen')[0].value
    console.log(maximaleBestellungne)
    let optimale_bestellmengen = [];
    let aktuelle_bestellmenge = bestellmenge;
    
    for (let bestellung_nr = 1; bestellung_nr <= maximaleBestellungne; bestellung_nr++) {
        aktuelle_bestellmenge = bestellmenge / bestellung_nr
        let optimale_bmenge = {}; 
        if (aktuelle_bestellmenge < 1) {
            break;
        }
        optimale_bmenge['Bestellungsnummer'] = bestellung_nr;
        optimale_bmenge['Bestellungsmenge'] = Number(aktuelle_bestellmenge).toFixed(2);
        optimale_bmenge['Bestellungskosten'] = Number(bestellkosten * bestellung_nr).toFixed(2)
        optimale_bmenge['Durschn.Lagerbestand'] = Number(aktuelle_bestellmenge / 2).toFixed(2);
        optimale_bmenge['Lagerhaltungskosten'] = Number(aktuelle_bestellmenge / 2 * lagerkosten).toFixed(2);
        optimale_bmenge['Kosten gesamt'] = Number(Number(optimale_bmenge['Lagerhaltungskosten']) + Number(optimale_bmenge['Bestellungskosten'])  ).toFixed(2);
        optimale_bestellmengen.push(optimale_bmenge);
    }
    return optimale_bestellmengen;
}


function create_optimale_table() {

    let url = window.location.origin + '/api/produkt/';

    fetch(url)
        .then(
            function (response) {
                if (response.status !== 200) {
                    console.log('Error wihle trying too contatc the REST API');
                    return;
                }
                response.json().then(function (data) {
                    var produkt_data = data.results
                    var table = $('#optimale').DataTable({
                        data: produkt_data,
                        columns: [
                            {
                                "className": 'details-control',
                                "orderable": false,
                                "data": null,
                                "defaultContent": '<img src="' + window.location.origin + '/static/picture/add-plus-button.png' + '"/>'
                            },
                            { data: "id" },

                            { data: "name" },
                            { data: "ek_preis" },
                            { data: "list_preis" },
                            { data: "beschreibung" },
                            { data: "lagerkosten" },
                            { data: "bestellkosten" },
                            { data: "erdat" },
                        ],
                        order: [[1, "desc"]],
                    });


                    $('#optimale tbody').on('click', 'td.details-control', function () {
                        let tr = $(this).closest('tr');
                        let row = table.row(tr);

                        if (row.child.isShown()) {
                            // This row is already open - close it
                            row.child.hide();
                            document.querySelectorAll('.shown')[0].cells[0].innerHTML = '<img src="' + window.location.origin + '/static/picture/add-plus-button.png' + '"/>'
                            tr.removeClass('shown');
                        }
                        else {
                            // Open this row
                            row.child(format(row.data())).show();
                            var optimale_chart = new optimale_Grafik('div_canvas', 100, 200)
                            console.log(bestellmengen)
                            optimale_chart.create_line_chart(bestellmengen)

                            close_all_other_rows();
                            tr.addClass('shown');
                            let plus_row = document.querySelectorAll('.shown');
                            plus_row[0].cells[0].innerHTML = '<img src="' + window.location.origin + '/static/picture/substract.png' + '"/>'
                            bedingteFormatierung();

                        }
                    })

                });
            })
        .catch(function (err) {
            console.log('Fetch Error :-S', err)
        });

}


function bedingteFormatierung() {

    let max_value = Number(document.querySelector('.for_color_calu').innerHTML);
    let min_value = Number(document.querySelector('.for_color_calu').innerHTML);


    document.querySelectorAll('.for_color_calu').forEach(kosten => {
        if (kosten.innerHTML > max_value) {
            max_value = Number(kosten.innerHTML);

        } else if (kosten.innerHTML < min_value) {
            min_value = Number(kosten.innerHTML);
        }
    });


    document.querySelectorAll('.cf tbody tr').forEach(e => {

        e.style.background = getColor(e.getAttribute('data-value'), max_value, min_value);
    });

}



function close_all_other_rows() {

    let elemtent_to_close = document.querySelectorAll('.shown');
    if (elemtent_to_close.length != 0) {
        elemtent_to_close[0].cells[0].innerHTML = '<img src="' + window.location.origin + '/static/picture/add-plus-button.png' + '"/>'
        elemtent_to_close[0].nextSibling.remove()
        elemtent_to_close[0].classList.remove('shown')
    };
}


class optimale_Grafik extends Grafik {

    constructor(div_id, widht, hight) {
        super(div_id, widht, hight)
    }

    create_datasets(data) {
        this.datasets = []
        this.lables = []
        this.datasets.push(this.create_dataset('rgba(255,99,132,1)', 'Kosten gesamt', data, 'Kosten gesamt'))
        this.lables.push('Kosten gesamt')
        this.datasets.push(this.create_dataset('rgba(255, 206, 86, 1)', 'Bestellmenge', data, 'Bestellmenge'))
        this.lables.push('Bestellmenge')
        this.datasets.push(this.create_dataset('rgba(75, 192, 192, 1)', 'Bestellungskosten', data, 'Bestellungskosten'))
        this.lables.push('Bestellungskosten')
        this.datasets.push(this.create_dataset('rgba(153, 102, 255, 1)', 'Lagerhaltungskosten', data, 'Lagerhaltungskosten'))
        this.lables.push('Lagerhaltungskosten')
        this.datasets.push(this.create_dataset('rgba(255, 159, 64, 1)', 'Durschn.Lagerbestand', data, 'Durschn.Lagerbestand'))
        this.lables.push('Durschn.Lagerbestand')
    }

    create_line_chart(data) {

        this.create_datasets(data)
        this.line_chart = new Chart(this.html_object, {
            type: 'line',
            lables: this.lables,
            data: {
                datasets: this.datasets
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],

                    xAxes: [{
                        type: 'linear',
                        position: 'bottom'
                    }]
                },
            }
        });

    }

}