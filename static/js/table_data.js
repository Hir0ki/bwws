class Table_data {

    /**
     * 
     * @param {Array of Object} data 
     * @param {Function} formatter function that is run before the data getter method
     */
    constructor(para_data, formatter) {
        this.data = para_data;
        this.format_data = formatter;
    }

    get data() {
        if (typeof this.format_data === "function") {
            return formatter(this.data)
        } else {
            return this.internal_data
        }
    }

    set data(data) {
        log("Data")
        if (typeof data === "object") {
            data.forEach(entry => {
                if (typeof entry !== "object") {
                    throw "Table data wrong set"
                }
            })
        } else {
            throw "Table data wrong set"
        }
        this.internal_data = data
    }

    /**
     * 
     * @param {String} name 
     * @param {Any Primitive Datatype} init_value 
     */
    add_new_column_data(name, init_value) {
        if (init_value != undefined) {
            this.internal_data.forEach(entry => {
                entry[name] = init_value
            })
        }
    }

}