class Table {

    constructor(table_id, columns, api_url) {
        this._fetch_data(api_url).then(() => {
            log('Apirequest done');
            this.table_id = table_id;
            this.columns = columns;
            this._get_table_html()
            this.clean_table_data()
            this._create_table(this.table_id, this.columns)
        })
    }

    async _fetch_data(api_url) {
        let url = window.location.origin + api_url
        const response = await fetch(url);
        const json = await response.json();
        this.api_data = await json.results
    }

    _create_table() {
        log("Creating new table")
        this.table = $('#' + this.table_id).DataTable({
            data: this.table_data.data,
            columns: this.columns
        });
        log("created table")
    }

    _get_table_html() {
        let o = {}
        o = document.querySelector('#' + this.table_id)
        this.html_table = o

    }

    _create_new_html_table() {
        while (this.html_table.firstChild) {
            this.html_table.removeChild(this.html_table.firstChild);
        }
        document.querySelector('#table_area').appendChild(this.html_table)
    }
    /**
     * is run before table is initalised so should be reimplemented if you want do change api data
     */
    clean_table_data() {
        this.table_data = new Table_data(this.api_data);
    }

    refresh_table() {
        this.table.destroy(true)
        this._create_new_html_table()
        this.clean_table_data()
        this._create_table()
    }



    /**
     * 
     * @param {Object} column neues column object siehe DataTable doc
     * @param {*} column_data 
     */
    add_column(column, column_data, create_init_data) {

        if (column != undefined) {
            this.columns.push(column)

            if (column_data == undefined) {
                column_data = ' '
            }
            console.log(create_init_data, column_data, column)
            if (create_init_data == undefined) {
                this.table_data.add_new_column_data(column['data'], column_data)
            }
        }

        this.refresh_table()

    }


}