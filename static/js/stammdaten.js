// locale event handler

$(document).ready(function () {
    create_stammdaten_table();
});
function create_stammdaten_table() {
    var table = $("#produkt").DataTable()
    $("#tabelle").change(function () {
        $("select option:selected").each(function () {

            if ($(this).text() == "Lieferant") {

                document.getElementById("produkt").style.display = "none";
                document.getElementById("lieferant").style.display = "table";

                table.destroy();
                let url = window.location.origin + '/api/lieferant/';
                fetch(url)
                    .then(
                        function (response) {
                            if (response.status !== 200) {
                                console.log('Error wihle trying too contatc the REST API');
                                return;
                            }
                            response.json().then(function (data) {
                                var t_data = data.results
                                table = $('#lieferant').DataTable({
                                    data: t_data,
                                    columns: [
                                        { data: "id" },
                                        { data: "name" },
                                        { data: "rabatt" },
                                        { data: "skonto" },
                                        { data: "erdat" },
                                    ],
                                    order: [[1, "desc"]],
                                });
                            });
                        })
                    .catch(function (err) {
                        console.log('Fetch Error :-S', err)
                    });

            } else if ($(this).text() == "Produkt") {

                document.getElementById("lieferant").style.display = "none";
                document.getElementById("produkt").style.display = "table";


                table.destroy();
                let url = window.location.origin + '/api/produkt/';
                fetch(url)
                    .then(
                        function (response) {
                            if (response.status !== 200) {
                                console.log('Error wihle trying too contatc the REST API');
                                return;
                            }
                            response.json().then(function (data) {
                                var t_data = data.results
                                table = $('#produkt').DataTable({
                                    data: t_data,
                                    columns: [
                                        { data: "id" },

                                        { data: "name" },
                                        { data: "ek_preis" },
                                        { data: "list_preis" },
                                        { data: "beschreibung" },
                                        { data: "verbrauchsmenge" },
                                        { data: "lagerkosten" },
                                        { data: "bestellkosten" },
                                        { data: "bestand"},
                                        { data: "erdat" },
                                    ],
                                    order: [[1, "desc"]],
                                });
                            });
                        })
                    .catch(function (err) {
                        console.log('Fetch Error :-S', err)
                    });

            } else if ($(this).text() == "-") {

                document.getElementById("lieferant").style.display = "none";
                document.getElementById("produkt").style.display = "none";

            }
        });
    })
}
