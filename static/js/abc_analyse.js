var table_data

$(document).ready(function () {
    create_quali_table();

    document.getElementById('display_grafik').addEventListener('click', () => {
        if (document.getElementById('div_canvas').childElementCount == 0) {
            let option = document.querySelector('#abc_type').value
            var abc_grafik = new ABC_Grafik('div_canvas', 200, 100, table_data)
            abc_grafik.convert_table_data_to_chart_dataset(table_data, option, 'name')
            abc_grafik.create_bar_chart(option)
        } else {

        }
    })

});

document.querySelector('#update_button').addEventListener('click', (e) => {
    location.reload();

})

class ABC_Grafik extends Grafik {

    constructor(div_id, widht, hight) {
        super(div_id, widht, hight)
    }

    convert_table_data_to_chart_dataset(table_data, key_data, key_lable) {
        this.lables = []
        this.dataset = []
        this.backgroundColor = []
        this.borderColor = []
        table_data.forEach(entry => {
            this.lables.push(entry[key_lable])
            this.dataset.push(Number(entry[key_data]))

            switch (entry['ABC']) {
                case 'A':
                    this.backgroundColor.push('rgba(255, 99, 132, 0.2)')
                    this.borderColor.push('rgba(255,99,132,1)')
                    break;
                case 'B':
                    this.backgroundColor.push('rgba(54, 162, 235, 0.2)')
                    this.borderColor.push('rgba(54, 162, 235, 1)')
                    break;

                case 'C':
                    this.backgroundColor.push('rgba(255, 206, 86, 0.2)')
                    this.borderColor.push('rgba(255, 206, 86, 1)')
                    break;
                default:
                    this.backgroundColor.push('rgba(75, 192, 192, 0.2)')
                    this.borderColor.push('rgba(75, 192, 192, 1)')
                    break;
            }

        })
    }


    create_bar_chart(option) {
        let lable
        if (option == 'menge_kum') {
            lable = 'kumulierte Menge in %'
        } else {
            lable = 'kumulierte Umatz in %'
        }

        console.log(this.lables)
        this.bar_chart = new Chart(this.html_object, {
            type: 'bar',
            data: {
                labels: this.lables,
                datasets: [{
                    label: lable,
                    data: this.dataset,
                    backgroundColor: this.backgroundColor,
                    borderColor: this.borderColor,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

    }

}

var create_quali_table = () => {
    let url = window.location.origin + '/api/produkt/';
    fetch(url)
        .then(
            (response) => {
                if (response.status !== 200) {
                    console.log('Error wihle trying too contatc the REST API');
                    return;
                }
                response.json().then((data) => {
                    table_data = clean_data_for_display(data)
                    table_data = compute_data_for_abc_analyse(table_data)

                    convert_data_entrys_to_number(table_data, ['list_preis', 'umsatz', 'umsatz_pro', 'umsatz_kum',
                        'menge_pro', 'menge', 'menge_kum'], 2)

                    var table = $('#abc_table').DataTable({
                        data: table_data,
                        columns: [
                            {
                                data: "id",
                                title: "Produkt"
                            },

                            {
                                data: "name",
                                title: "Name"
                            },

                            {
                                data: "verbrauchsmenge",
                                title: "Verbrauch"
                            },

                            {
                                data: "list_preis",
                                title: "Preis",
                                render: function (data, type, row) {
                                    return data + ' €';
                                }
                            },

                            {
                                data: "umsatz",
                                title: "Umsatz in €",
                                render: function (data, type, row) {
                                    return data + ' €';
                                }
                            },

                            {
                                data: "umsatz_pro",
                                title: "Umsatz in %",
                                render: function (data, type, row) {
                                    return data + ' %';
                                }
                            },

                            {
                                data: "umsatz_kum",
                                title: "kum Umsatz in %",
                                render: function (data, type, row) {
                                    return data + ' %';
                                }
                            },

                            {
                                data: "menge_pro",
                                title: " Menge in %",
                                render: function (data, type, row) {
                                    return data + ' %';
                                }
                            },

                            {
                                data: "menge_kum",
                                title: "kum Menge in %",
                                render: function (data, type, row) {
                                    return data + ' %';
                                }
                            },

                            {
                                data: "ABC",
                                title: "A, B, C",
                            },


                        ]
                    })


                });
            })
        .catch(function (err) {
            console.log('Fetch Error :-S', err)
        });

}

function clean_data_for_display(data) {
    let new_data = []
    data.results.forEach(entry => {
        let new_entry = {};
        new_entry['id'] = entry['id']
        new_entry['name'] = entry['name']
        new_entry['list_preis'] = entry['list_preis']
        new_entry['verbrauchsmenge'] = entry['verbrauchsmenge']
        new_data.push(new_entry)
    });
    return new_data
}

function compute_data_for_abc_analyse(data) {
    let new_data = []
    let index = 0;
    //calculate sum of menge and umsatz 
    data.forEach(entry => {

        entry['umsatz'] = Number(entry['list_preis'] * entry['verbrauchsmenge'])

        if (entry['umsatz_sum'] == undefined) {
            entry['umsatz_sum'] = Number(entry['umsatz'])
        }
        if (entry['menge_sum'] == undefined) {
            entry['menge_sum'] = Number(entry['verbrauchsmenge'])
        }

        if (index != 0) {
            entry['menge_sum'] = Number(new_data[index - 1]['menge_sum']) + Number(entry['verbrauchsmenge'])
            entry['umsatz_sum'] = Number(new_data[index - 1]['umsatz_sum']) + Number(entry['umsatz'])
        }


        new_data.push(entry)
        index++;
    });
    index = 0
    // calculate umsatz and menege in prozent
    new_data.forEach(entry => {
        entry['umsatz_pro'] = Number(Number(entry['umsatz']) / Number(new_data[new_data.length - 1]['umsatz_sum']) * 100)
        entry['menge_pro'] = Number(Number(entry['verbrauchsmenge']) / Number(new_data[new_data.length - 1]['menge_sum']) * 100)
        new_data[index] = entry
        index++;
    });

    new_data = sort_data_table(new_data, 'umsatz', true)

    index = 0;
    //calculate kumultiver umsatz and menge
    new_data.forEach(entry => {

        if (index != 0) {
            entry['umsatz_kum'] = Number(Number(entry['umsatz_pro']) + Number(new_data[index - 1]['umsatz_kum']))
            entry['menge_kum'] = Number(Number(entry['menge_pro']) + Number(new_data[index - 1]['menge_kum']))
        } else {
            entry['umsatz_kum'] = entry['umsatz_pro']
            entry['menge_kum'] = entry['menge_pro']
        }
        index++
    })

    do_abc_calc(new_data)

    return new_data
}

var do_abc_calc = (data) => {

    let a_limit = Number(document.getElementById('a_limit').value)
    let b_limit = Number(document.getElementById('b_limit').value)

    data.forEach(entry => {
        option = document.querySelector('#abc_type').value

        if (Number(entry[option]) < a_limit) {
            entry['ABC'] = 'A';
        } else if (Number(entry[option]) < b_limit) {
            entry['ABC'] = 'B'
        } else {
            entry['ABC'] = 'C'
        }

    })

}


